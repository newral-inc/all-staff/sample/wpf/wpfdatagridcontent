﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WPFDataGridContent
{
    /**
     * @brief セルデータクラス
     *        ユーザが定義するクラス
     */
    class CellData
    {
        public string Name { get; set; }
        public string Data1 { get; set; }
        public string Data2 { get; set; }
        public override string ToString()
        {
            return Name;
        }
    }

    /**
     * @brief セルアイテムクラス
     *        DataGridに設定するクラス
     */
    class CellItem
    {
        public CellData CellData { get; set; }
    }

    /// <summary>
    /// MainWindow.xaml の相互作用ロジック
    /// </summary>
    public partial class MainWindow : Window
    {
        /**
         * @brief コンストラクタ
         */
        public MainWindow()
        {
            InitializeComponent();

            // DataGridにデータを設定します。
            var items = new List<CellItem>();
            items.Add(new CellItem() { CellData = new CellData() { Name = "セルデータ1", Data1 = "データ11", Data2 = "データ12" } } );
            items.Add(new CellItem() { CellData = new CellData() { Name = "セルデータ2", Data1 = "データ21", Data2 = "データ22" } });
            items.Add(new CellItem() { CellData = new CellData() { Name = "セルデータ3", Data1 = "データ31", Data2 = "データ32" } });
            dataGrid.ItemsSource = items;
        }

        /**
         * @brief ウィンドウが描画された後に呼び出されます。
         * 
         * @param [in] sender ウィンドウ
         * @param [in] e イベント
         */
        private void Window_ContentRendered(object sender, EventArgs e)
        {
            // 行0, 列0のDataGridCellを取得します。
            var cell = GetDataGridCell(dataGrid, 0, 0);

            // DataGridCellのDataContextを取得します。
            // DataContextにCellItem型のオブジェクトが設定されています。
            var cellItem = cell.DataContext as CellItem;

            // 以下、CellItem型のオブジェクトとして処理します。
            Console.WriteLine(cellItem);
        }

        // 内部メソッド(詳細は省略)

        public DataGridCell GetDataGridCell(DataGrid dataGrid, int rowIndex, int columnIndex)
        {
            if (dataGrid.Items == null || dataGrid.Items.IsEmpty)
            {
                return null;
            }

            var row = GetDataGridRow(dataGrid, rowIndex);
            if (row == null)
            {
                return null;
            }

            var presenter = GetVisualChild<DataGridCellsPresenter>(row);
            if (presenter == null)
            {
                return null;
            }

            var generator = presenter.ItemContainerGenerator;
            var cell = generator.ContainerFromIndex(columnIndex) as DataGridCell;
            if (cell == null)
            {
                dataGrid.UpdateLayout();
                var column = dataGrid.Columns[columnIndex];
                dataGrid.ScrollIntoView(row, column);
                cell = generator.ContainerFromIndex(columnIndex) as DataGridCell;
            }
            return cell;
        }

        public DataGridRow GetDataGridRow(DataGrid dataGrid, int index)
        {
            if (dataGrid.Items == null || dataGrid.Items.IsEmpty)
            {
                return null;
            }

            var generator = dataGrid.ItemContainerGenerator;
            var row = generator.ContainerFromIndex(index) as DataGridRow;
            if (row == null)
            {
                dataGrid.UpdateLayout();
                var item = dataGrid.Items[index];
                dataGrid.ScrollIntoView(item);
                row = generator.ContainerFromIndex(index) as DataGridRow;
            }
            return row;
        }

        private T GetVisualChild<T>(Visual parent) where T : Visual
        {
            T result = default(T);
            var count = VisualTreeHelper.GetChildrenCount(parent);
            for (int i = 0; i < count; ++i)
            {
                var child = VisualTreeHelper.GetChild(parent, i) as Visual;
                result = child as T;
                if (result != null)
                {
                    break;
                }

                result = GetVisualChild<T>(child);
            }
            return result;
        }
    }
}
